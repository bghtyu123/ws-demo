package com.synjones.websocket.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

/**
 * @title: CheckinController
 * @description: 签到控制器
 * @author: Walter
 * @create: 2020/3/30 17:03
 * @version: 1.0
 **/
@RestController
@RequestMapping("/checkin")
public class CheckinController {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @GetMapping("/test")
    public Map<String, Object> test() {
        Map<String, Object> res = new TreeMap<>();
        Random r = new Random();
        String studentId = String.valueOf(100000000 + r.nextInt(899999999));
        res.put("studentId", studentId);
        res.put("msg", "You have checked in!");

        try {
            Map<String, Object> message = new HashMap<>();
            message.put("studentId", studentId);
            ObjectMapper mapper = new ObjectMapper();

            String msgJson = mapper.writeValueAsString(message);
            messagingTemplate.convertAndSend("/topic/teacher/123", msgJson);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return res;
    }
}
